# plantuml-styles

PlantUML styles

Stylesheets for PlantUML: PlantUML is pretty versatile and frankly speaking, awesome piece of software, but it has huge problem: it's ugly! Maroon and light-yellow styled graphs look so ancient.

Now you can pick one from four color skins in two versions: Dark and Light.

How to use:

@startuml
!define DARKRED
!includeurl https://bitbucket.org/beeyou/plantuml-styles/raw/master/styles.puml
Alice -> Bob[[http://plantuml.com/sequence]]: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
@enduml